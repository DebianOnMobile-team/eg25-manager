/*
 * Copyright (C) 2020 Arnaud Ferraris <arnaud.ferraris@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"
#include "gpio.h"

#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>

/* Those defines are used for legacy config files only */
#define GPIO_CHIP1_LABEL "1c20800.pinctrl"
#define GPIO_CHIP2_LABEL "1f02c00.pinctrl"
#define MAX_GPIOCHIP_LINES 352

enum {
    GPIO_OUT_DTR = 0,
    GPIO_OUT_PWRKEY,
    GPIO_OUT_RESET,
    GPIO_OUT_APREADY,
    GPIO_OUT_DISABLE,
    GPIO_OUT_COUNT
};

enum {
    GPIO_IN_STATUS = 0,
    GPIO_IN_COUNT
};

static char *gpio_out_names[] = {
    "dtr",
    "pwrkey",
    "reset",
    "apready",
    "disable",
};

static char *gpio_in_names[] = {
    "status",
};

enum gpiod_line_value gpio_line_get_value(struct EG25Manager *manager, int line) {
    enum gpiod_line_value value;
    unsigned int offset;

    gpiod_line_request_get_requested_offsets(manager->gpio_in[line], &offset, 1);
    value = gpiod_line_request_get_value(manager->gpio_in[line], offset);

    if (value == GPIOD_LINE_VALUE_ERROR) {
            g_warning("gpio: couldn't get value on line %d", line);
    }

    return value;
}

int gpio_line_set_value(struct EG25Manager *manager, int line, enum gpiod_line_value value) {
    unsigned int offset;
    int ret;

    gpiod_line_request_get_requested_offsets(manager->gpio_out[line], &offset, 1);
    ret = gpiod_line_request_set_value(manager->gpio_out[line], offset, value);
    if (ret) {
        g_warning("gpio: couldn't set value %d on line %d", value, line);
        return -1;
    }
    else {
        manager->gpio_out_value[line] = value;
        return 0;
    }
}

int gpio_sequence_poweron(struct EG25Manager *manager)
{
    /*
     * Force the modem to poweroff using the RESET_N pin before attempting to
     * boot in case the it got into a bad state.
     *
     * If the modem was on, this will cause it to start booting, so press the
     * power button while in reset to avoid a (probably only theoretical) race
     * condition where it starts booting after reset, and then powers off from
     * the power key.
     */
    gpio_line_set_value(manager, GPIO_OUT_RESET, GPIOD_LINE_VALUE_ACTIVE);
    gpio_line_set_value(manager, GPIO_OUT_PWRKEY, GPIOD_LINE_VALUE_ACTIVE);

    /*
     * The datasheet says to pull the pin low for between 150 and 460 ms. usleep
     * should always sleep for at least the specified amount of time, so use
     * 200ms because it's closer to the bottom of that range.
     */
    usleep(200000);

    gpio_line_set_value(manager, GPIO_OUT_RESET, GPIOD_LINE_VALUE_INACTIVE);

    /*
     * The modem has finished it's reset, now we wait to allow it a chance to
     * react to the power key
     */
    sleep(1);

    gpio_line_set_value(manager, GPIO_OUT_PWRKEY, GPIOD_LINE_VALUE_INACTIVE);

    g_message("Executed power-on sequence");

    return 0;
}

int gpio_sequence_shutdown(struct EG25Manager *manager)
{
    gpio_line_set_value(manager, GPIO_OUT_PWRKEY, GPIOD_LINE_VALUE_ACTIVE);
    sleep(1);
    gpio_line_set_value(manager, GPIO_OUT_PWRKEY, GPIOD_LINE_VALUE_INACTIVE);

    g_message("Executed power-off sequence");

    return 0;
}

int gpio_sequence_suspend(struct EG25Manager *manager)
{
    gpio_line_set_value(manager, GPIO_OUT_APREADY, GPIOD_LINE_VALUE_ACTIVE);

    g_message("Executed suspend sequence");

    return 0;
}

int gpio_sequence_resume(struct EG25Manager *manager)
{
    gpio_line_set_value(manager, GPIO_OUT_APREADY, GPIOD_LINE_VALUE_INACTIVE);

    g_message("Executed resume sequence");

    return 0;
}

int gpio_sequence_wake(struct EG25Manager *manager)
{
    if (manager->gpio_out_value[GPIO_OUT_DTR]) {
        gpio_line_set_value(manager, GPIO_OUT_DTR, GPIOD_LINE_VALUE_INACTIVE);

        /* Give the modem 200ms to wake from soft sleep */
        usleep(200000);

        g_message("Executed soft wake sequence");
    }

    return 0;
}

int gpio_sequence_sleep(struct EG25Manager *manager)
{
    gpio_line_set_value(manager, GPIO_OUT_DTR, GPIOD_LINE_VALUE_ACTIVE);
    g_message("Executed soft sleep sequence");

    return 0;
}

struct gpiod_line_request *gpio_request_line(struct EG25Manager *manager, int chip, unsigned int line, enum gpiod_line_direction direction) {
    struct gpiod_line_request *request = NULL;
    struct gpiod_line_settings *settings;
    struct gpiod_line_config *line_cfg;
    struct gpiod_request_config *req_cfg;
    int ret;

    settings = gpiod_line_settings_new();
    if (!settings)
        return NULL;

    gpiod_line_settings_set_direction(settings, direction);

    line_cfg = gpiod_line_config_new();
    if (!line_cfg)
        goto free_settings;

    ret = gpiod_line_config_add_line_settings(line_cfg, &line, 1, settings);
    if (ret)
        goto free_line_config;

    req_cfg = gpiod_request_config_new();
    if (!req_cfg)
        goto free_line_config;

    gpiod_request_config_set_consumer(req_cfg, "eg25-manager");

    request = gpiod_chip_request_lines(manager->gpiochip[chip], req_cfg, line_cfg);

    gpiod_request_config_free(req_cfg);

free_line_config:
    gpiod_line_config_free(line_cfg);

free_settings:
    gpiod_line_settings_free(settings);

    return request;
}

static int gpio_chip_dir_filter(const struct dirent *entry)
{
    struct stat sb;
    int ret = 0;
    char *path;

    if (asprintf(&path, "/dev/%s", entry->d_name) < 0)
        return 0;

    if ((lstat(path, &sb) == 0) && (!S_ISLNK(sb.st_mode)) &&
        gpiod_is_gpiochip_device(path))
        ret = 1;

    free(path);

    return ret;
}

int gpio_all_chip_paths(char ***paths_ptr)
{
    int i, j, num_chips, ret = 0;
    struct dirent **entries;
    char **paths;

    num_chips = scandir("/dev/", &entries, gpio_chip_dir_filter, alphasort);
    if (num_chips < 0)
        g_error("gpio: unable to scan /dev: %s", g_strerror(errno));

    paths = calloc(num_chips, sizeof(*paths));
    if (paths == NULL)
        g_error("gpio: out of memory");

    for (i = 0; i < num_chips; i++) {
        if (asprintf(&paths[i], "/dev/%s", entries[i]->d_name) < 0) {
            for (j = 0; j < i; j++)
                free(paths[j]);

            free(paths);
            return 0;
        }
    }

    *paths_ptr = paths;
    ret = num_chips;

    for (i = 0; i < num_chips; i++)
        free(entries[i]);

    free(entries);
    return ret;
}

struct gpiod_chip *gpio_chip_open_by_label(const char *label)
{
    int num_chips, i;
    char **paths;
    const char *clabel;
    struct gpiod_chip *chip;
    struct gpiod_chip_info *cinfo;

    num_chips = gpio_all_chip_paths(&paths);

    for (i = 0; i < num_chips; i++) {
        chip = gpiod_chip_open(paths[i]);
        if (!chip)
            continue;

        cinfo = gpiod_chip_get_info(chip);
        if (!cinfo)
            goto clean_chip_open;

        clabel = gpiod_chip_info_get_label(cinfo);

        if (strcmp(label, clabel) == 0) {
            return chip;
        }

clean_chip_open:
        gpiod_chip_close(chip);
    }

    return NULL;
}

unsigned int gpio_chip_num_lines(struct EG25Manager *manager, unsigned int chip_num) {
    struct gpiod_chip *chip = manager->gpiochip[chip_num];
    struct gpiod_chip_info *info;
    unsigned int num_lines;

    info = gpiod_chip_get_info(chip);
    if (!info)
        g_error("gpio: failed to read info: %s", strerror(errno));

    num_lines = gpiod_chip_info_get_num_lines(info);

    gpiod_chip_info_free(info);

    return num_lines;
}

int gpio_init(struct EG25Manager *manager, toml_table_t *config[])
{
    int i;
    toml_table_t *gpio_config[EG25_CONFIG_COUNT];

    for (i = 0; i < EG25_CONFIG_COUNT; i++)
        gpio_config[i] = config[i] ? toml_table_in(config[i], "gpio") : NULL;

    if (!gpio_config[EG25_CONFIG_SYS])
        g_error("Default config file lacks the 'gpio' section!");

    /*
     * The system config could have the `chips` key, but the user one
     * could still use the old format! In order to avoid problems, we
     * should use the new format only if:
     *   - there's no user config file
        or
     *   - the user config file contains the `chips` key
     * Otherwise we might start parsing the system config with the new
     * format, but error out if user config overrides gpios using the
     * old format
     */
    if (!gpio_config[EG25_CONFIG_USER] || toml_array_in(gpio_config[EG25_CONFIG_USER], "chips"))
    {
        int numchips;
        toml_array_t *chipslist = NULL;

        config_get_array(gpio_config, "chips", &chipslist);
        numchips = toml_array_nelem(chipslist);
        if (numchips > 2)
            g_error("Requesting too many GPIO chips!");

        for (i = 0; i < numchips; i++) {
            toml_datum_t data = toml_string_at(chipslist, i);
            if (!data.ok)
                continue;
            manager->gpiochip[i] = gpio_chip_open_by_label(data.u.s);
            if (!manager->gpiochip[i])
                g_error("Unable to find GPIO chip '%s'", data.u.s);
        }

        for (i = 0; i < GPIO_OUT_COUNT; i++) {
            toml_table_t *table;
            toml_datum_t chip, line;
            if (!config_get_table(gpio_config, gpio_out_names[i], &table))
                g_error("Unable to get config for output GPIO '%s'", gpio_out_names[i]);

            chip = toml_int_in(table, "chip");
            if (!chip.ok || chip.u.i < 0 || chip.u.i > 2)
                g_error("Wrong chip ID for output GPIO '%s'", gpio_out_names[i]);

            line = toml_int_in(table, "line");
            if (!line.ok || line.u.i < 0 || line.u.i > gpio_chip_num_lines(manager, chip.u.i))
                g_error("Wrong line ID for output GPIO '%s'", gpio_out_names[i]);

            manager->gpio_out[i] = gpio_request_line(manager, chip.u.i, line.u.i, GPIOD_LINE_DIRECTION_OUTPUT);
            if (!manager->gpio_out[i])
                g_error("Unable to get output GPIO %d", i);
        }

        for (i = 0; i < GPIO_IN_COUNT; i++) {
            toml_table_t *table;
            toml_datum_t chip, line;

            if (!config_get_table(gpio_config, gpio_in_names[i], &table)) {
                // BH edition don't have the STATUS line connected, ignore it
                if (manager->use_libusb && g_strcmp0(gpio_in_names[i], "status") == 0)
                    continue;
                g_error("Unable to get config for input GPIO '%s'", gpio_in_names[i]);
            }

            chip = toml_int_in(table, "chip");
            if (!chip.ok || chip.u.i < 0 || chip.u.i > 2)
                g_error("Wrong chip ID for input GPIO '%s'", gpio_in_names[i]);

            line = toml_int_in(table, "line");
            if (!line.ok || line.u.i < 0 || line.u.i > gpio_chip_num_lines(manager, chip.u.i))
                g_error("Wrong line ID for input GPIO '%s'", gpio_in_names[i]);

            manager->gpio_in[i] = gpio_request_line(manager, chip.u.i, line.u.i, GPIOD_LINE_DIRECTION_INPUT);
            if (!manager->gpio_in[i])
                g_error("Unable to get input GPIO %d", i);
        }
    } else {
        guint offset, chipidx, gpio_idx;

        /* Legacy config file, only used on the OG PinePhone */
        manager->gpiochip[0] = gpio_chip_open_by_label(GPIO_CHIP1_LABEL);
        if (!manager->gpiochip[0])
            g_error("Unable to open GPIO chip " GPIO_CHIP1_LABEL);

        manager->gpiochip[1] = gpio_chip_open_by_label(GPIO_CHIP2_LABEL);
        if (!manager->gpiochip[1])
            g_error("Unable to open GPIO chip " GPIO_CHIP2_LABEL);

        for (i = 0; i < GPIO_OUT_COUNT; i++) {
            if (!config_get_uint(gpio_config, gpio_out_names[i], &gpio_idx))
                g_error("Unable to get config for output GPIO '%s'", gpio_out_names[i]);

            if (gpio_idx < MAX_GPIOCHIP_LINES) {
                offset = gpio_idx;
                chipidx = 0;
            } else {
                offset = gpio_idx - MAX_GPIOCHIP_LINES;
                chipidx = 1;
            }

            manager->gpio_out[i] = gpio_request_line(manager, chipidx, offset, GPIOD_LINE_DIRECTION_OUTPUT);
            if (!manager->gpio_out[i])
                g_error("Unable to get output GPIO %d", i);
        }

        for (i = 0; i < GPIO_IN_COUNT; i++) {
            if (!config_get_uint(gpio_config, gpio_in_names[i], &gpio_idx))
                continue;

            if (gpio_idx < MAX_GPIOCHIP_LINES) {
                offset = gpio_idx;
                chipidx = 0;
            } else {
                offset = gpio_idx - MAX_GPIOCHIP_LINES;
                chipidx = 1;
            }

            manager->gpio_in[i] = gpio_request_line(manager, chipidx, offset, GPIOD_LINE_DIRECTION_INPUT);
            if (!manager->gpio_in[i])
                g_error("Unable to get input GPIO %d", i);
        }
    }

    return 0;
}

void gpio_force_off(struct EG25Manager *manager)
{
    if (manager->gpio_out[GPIO_OUT_RESET]) {
        g_message("Setting the reset pin to ensure the modem stays off");
        gpio_line_set_value(manager, GPIO_OUT_RESET, GPIOD_LINE_VALUE_ACTIVE);
    }
}

gboolean gpio_check_poweroff(struct EG25Manager *manager)
{
    if (manager->gpio_in[GPIO_IN_STATUS] &&
        gpio_line_get_value(manager, GPIO_IN_STATUS) == GPIOD_LINE_VALUE_ACTIVE) {
        return TRUE;
    }

    return FALSE;
}

void gpio_destroy(struct EG25Manager *manager)
{
    int i;

    for (i = 0; i < GPIO_OUT_COUNT; i++) {
        if (manager->gpio_out[i])
            gpiod_line_request_release(manager->gpio_out[i]);
    }

    for (i = 0; i < GPIO_IN_COUNT; i++) {
        if (manager->gpio_in[i])
            gpiod_line_request_release(manager->gpio_in[i]);
    }

    if (manager->gpiochip[0])
        gpiod_chip_close(manager->gpiochip[0]);
    if (manager->gpiochip[1])
        gpiod_chip_close(manager->gpiochip[1]);
}
